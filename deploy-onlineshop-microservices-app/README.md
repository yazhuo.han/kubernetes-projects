1. Start a linode k8s cluster,
download online-shop-microservices-kubeconfig.yaml
$ chomod 400 online-shop-microservices-kubeconfig.yaml

2. Set the kubeconfig file as environmental variable
$ export KUBECONFIG=~/online-shop-microservices-kubeconfig.yaml

3. create a namespace
$ kubectl create ns microservices

4. Deploy microservices to the namespace
$ kubectl apply -f config.yaml -n microservices

5. check our microservices
$ kubectl get pod -n microservices
$ kubectl get svc -n microservices

------------------------------------------------
Use helm chart to deploy microservices:
Create Helm Chart for the Microservices:
$ helm create microservices

Different parts expained:
Chart.yaml = meta info about chart
chart folder = chart dependencies
.helmignore = files you don't want to include in your helm chart
# helm chart can be built into an archive and be hosted on a helm repository.

templates folder = where the actual k8s yaml files (template files) are created
Now, we delete everything but deployment.yaml and service.yaml in template folder. And make those two empty.

values.yaml = actual values for the template files
we will make this file empty too.

then, we create value files for each services.
To check the template is working:
$ helm template -f email-service-values.yaml microservice
or 
$ helm template -f email-service-values.yaml --set appReplicas=3 microservice
or 
$ helm lint -f email-service-values.yaml microservie

Deploy email service:
$ helm install -f email-service-values.yaml emailservice microservice
# "emailservice" is the release mame

As for redis,
since we didn't develop redis ourselves, we will create a separate helm chart for redis cart service.

$ helm create redis

then we create a folder named "charts" and put both microservice chart and redis chart in it.

Now, deploy redis service
$ helm template -f values/redis-values.yaml charts/redis

$ helm install --dry-run -f values/redis-values.yaml rediscart charts/redis

Difference between --dry-run and template: --dry-run send files to k8s cluster, while template only validates it locally

Next, create install.sh file to deploy all the microservices.
$ chmod u+x install.sh
$ ./install.sh
$ chmod u+x uninstall.sh
$ ./uninstall.sh

----------------------------------------------------
Deploy using Helmfile
Declarative way for deploying helm charts
Define the desired state.
- Declare a definition of an entire k8s cluster
- Change specification depending on application or type of environment

install Helmfile tool:
$ brew install helmfile
$ helmfile sync
$ helmfile list
$ kubectl get pod

$ helmfile destroy

DevOps engineer will be the creator of the helm chart, and the developers are the users of our helm chart