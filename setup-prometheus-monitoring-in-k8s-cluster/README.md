1. Manually create all the yaml files for each of the Prometheus components.

2. Using an Operator:
Operator = manager of all Prometheus components
- Find Prometheus operator
- Deploy in k8s cluster

3. Using Helm chart to deply operator
helm chart is maintained by Helm community
- Helm: initial setup
- Operator: manage setup

$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
$ helm repo add stable https://charts.helm.sh/stable
$ helm repo update
$ helm install prometheus prometheus-community/kube-prometheus-stack
$ kubectl get pod

# the result of those commands:
1. monitoring stack setup
2. monitoring configuration done for your k8s cluster
    - worker nodes monitored
    - k8s components monitored

# To check the result:
$ kubectl get configmap
$ kubectl get secret
$ kubectl get crd

CRD: extension of Kubernetes API - custom resource definitions

$ kubectl get statefulset
$ kubectl describe statefulset <prometheus> > prom.yaml
$ kubectl describe statefulset <alertmaneger> > alert.yaml

$ kubectl get deployment
$ kubectl describe deployment <operator> > operator.yaml

$ kubectl get secret
$ kubectl get secret <secret-name> -o yaml > secret.yaml

# Access Grafana
$ kubectl get service
$ kubectl get deployment
$ kubectl get pod
$ kubectl logs <grafana> -c grafana --> port 3000
$ kubectl port-forward deployment/prometheus-grafana 3000
open the browser, localhost:3000

# Access Prometheus
$ kubectl get pod
$ kubectl port-forward <prometheus> 9090
open the browser, localhost:9090
